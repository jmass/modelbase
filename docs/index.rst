Documentation for modelbase
===========================

.. toctree::
   :numbered:
   :maxdepth: 1

   installation
   tutorial
   source/api-changes.ipynb
   source/feature-show.ipynb
   source/ratelaw-api.ipynb
   source/labelmodel-api.ipynb


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
