"""Write me."""


class Model:
    """Write me."""

    def __init__(self):
        self.meta_info["model"]["sbo"] = "SBO:0000624"  # flux balance framework

    def _create_sbml_bounds(self, sbml_model):
        """Create the compartments for the sbml model.

        Since modelbase does not yet support flux analysis
        these are just set to arbitray lower and upper limits.

        Parameters
        ----------
        sbml_model : libsbml.Model
        """
        for bound_id, bound_value in {
            "LOWER_LIMIT": -1000,
            "ZERO": 0,
            "UPPER_LIMIT": 1000,
        }.items():
            bound = sbml_model.createParameter()
            bound.setId(bound_id)
            bound.setValue(bound_value)
            bound.setConstant(True)
