# Changelog

## 0.4.0
* Added experimental subpackage for PDE simulations
* Added experimental SBML import/export
* Added ratelaw API

## 0.3.1
* Added MCA routines
* Added and improved several plotting routines
* Added context manager
* get_rates can now be called with arrays/lists/tuples
* Removed analysis class
* Removed simulator.get_rates function
* Deprecated analysis class
* Added deprecation warning for get_rates

## 0.3.0
* Introduced new API (see below)
* Updated example files to new API
* Added parser function for MetaCyc-style databases
* Added rudimentary SBML import and export functions (without simplesbml)
* Repacked Model and LabelModel into ode sub-package
* Added dummy sub-packages for pde, stochastic and stoichiometric models
* Added type annotation for most functions
* Rewrote compound, rate and algebraic module storage for ode.Model
* Added __str__ representation
* Added io operations for model parameters
* Rewrote algebraic module constructor
* Rewrote simulator interface
* Added convenience plotting functions to simulator class
* Added dry run on set_initial_conditions to properly catch model construction errors
* Introduced overwrite warnings for adding duplicate rates and algebraic modules
* Re-introduced simulator get_time function
* Added plot_selection convenience function
* Simulate now uses the last time point as t_end, if given
* Re-introduced get_compounds_by_reg_exp function
* Added warning on removing compounds that are still in reactions
* Added generate_model_source_code function for ode models
* Added label scope routine
* Added rudimentary stoichiometric models
* Added conversion function from ODE models to stoichiometric models
* Added integrator argument to Simulator class

### API changes
For a comparison to the old API, see the api_changes and tutorial notebooks in the docs.


## 0.2.5
* Added SBML export

## 0.2.4
* Documentation improved. Now available on readthedocs.io. Includes instructions to install sundials
* Analysis no longer a subclass of Model. Now dummy class with static methods.
* Suit of unit tests included.
* Dependencies corrected in setup.py
* Version to be released with submission of revised manuscript to Journal of Open Research Software

## 0.2.3
* Renamed set_reaction to add_reaction for consistency.
* Version to be released with manuscript submission to Journal of Open Research Software

## 0.2.2
* Bug fixed: simulate.getV and getRate did not pass time for time-dependent rate functions generated with modelbase.set_ratev


## 0.2.1
* Changed total concentration names in label models to base_name+"_total"
* Added print_stoichiometryMatrix function to return pandas dataframe
* Added set_reaction and set_reaction_v shortcut functions
* Allowed ParameterSet input as update method for parameter update
* Warning when overwriting parameters with ParameterSet.update()  


## 0.2.0
* Separated analysis methods from model class to analysis.py
* Unified Simulator class calls with constructor method modelbase.Simulator()
* Removed AlgmSimulate class
* Unified AlgmModel and Model classes
* Changed algebraic module construction


## 0.1.8
* bugfix: in LabelModel setting c=0 (no labels in this compound) led to an error, because the sum of all labels
had the same name as the compound. Fixed.
* verbosity can be passed to the assimulo solver.


## 0.1.7
Support for the differential equation solver sundials (CVODE)
through the python package [assimulo](http://www.jmodelica.org/assimulo).
Falls back to scipy.integrate.ode if assimulo cannot be loaded.

Brief installation instructions of sundials/assimulo (tested on Ubuntu 14.04 and 16.04 and MacOX X):
* Install sundials-2.6.0 [here](https://computation.llnl.gov/projects/sundials/sundials-software). The version is important. We did not get 3.0.0 to run. You will need cmake and ccmake for it.
Set compiler flag -fPIC.
* pip install assimulo
